# README #

Control and distribution of electricity

### What is this repository for? ###

* It's a project of control and distribution of electric energy
* This project presents the data model layer

### How do I get set up? ###

* For compile you will need maven 3, Java 8(jdk) and execute in bash(on root folder): 
```
#!bash
$ mvn clean install
```
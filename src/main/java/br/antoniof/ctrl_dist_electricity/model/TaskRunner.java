package br.antoniof.ctrl_dist_electricity.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by antonio on 26/12/17.
 */
public class TaskRunner {
    @Id
    private String id = "taskRunnerId";
    private boolean running;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}

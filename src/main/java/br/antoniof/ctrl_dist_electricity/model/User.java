package br.antoniof.ctrl_dist_electricity.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;

/**
 * @author antonio
 */
@Document(collection = "users")
public class User extends BaseEntity implements UserDetails {

    private Collection<Authority> authorities;
    private String username;
    private String password;

    private String firstname;
    private String lastname;
    private GeoJsonPoint location;
    private Double powerConsumptionPerMinute = 0D;

    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean isEnabled;

    public User(String id, Date createdAt, Date updatedAt) {
        super.setId(id);
        super.setCreatedAt(createdAt);
        super.setUpdatedAt(updatedAt);
    }

    public User(String id, Date createdAt, Date updatedAt, Collection<Authority> authorities, String username, String password, String firstname, String lastname, boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired, boolean isEnabled) {
        this(id, createdAt, updatedAt);
        this.authorities = authorities;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.isEnabled = isEnabled;
    }

    public User() {
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {

        this.lastname = lastname;
    }

    public void setAuthorities(Collection<Authority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public Collection<Authority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    @Override
    @Id
    public String getId() {
        return super.getId();
    }

    @Override
    public void setId(String id) {
        super.setId(id);
    }

    public Double getPowerConsumptionPerMinute() {
        return powerConsumptionPerMinute;
    }

    public void setPowerConsumptionPerMinute(Double powerConsumptionPerMinute) {
        this.powerConsumptionPerMinute = powerConsumptionPerMinute;
    }
}
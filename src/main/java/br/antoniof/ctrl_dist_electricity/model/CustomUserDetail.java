package br.antoniof.ctrl_dist_electricity.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author antonio
 */
public class CustomUserDetail extends User implements UserDetails {

    public CustomUserDetail(User user) {
        super(user.getId(), user.getCreatedAt(), user.getUpdatedAt(), user.getAuthorities(), user.getUsername(),
                user.getPassword(), user.getFirstname(),
                user.getLastname(), user.isAccountNonExpired(),
                user.isAccountNonLocked(), user.isCredentialsNonExpired(),
                user.isEnabled());
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
package br.antoniof.ctrl_dist_electricity.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author antonio
 */
@Document(collection = "storageEquipaments")
public class StorageEquipment {

	@Id
	private String id;
	private Double capacity = 1000D;
	private Double current = 0D;
	private Double loadPerMinute = 10D;
	private String name;
	private GeoJsonPoint location;
	@DBRef
	private GroupStorageEquipment groupStorageEquipment;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getCapacity() {
		return capacity;
	}

	public void setCapacity(Double capacity) {
		this.capacity = capacity;
	}

	public Double getCurrent() {
		return current;
	}

	public void setCurrent(Double current) {
		this.current = current;
	}

	public Double getLoadPerMinute() {
		return loadPerMinute;
	}

	public void setLoadPerMinute(Double loadPerMinute) {
		this.loadPerMinute = loadPerMinute;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void increase(){
		double sum = this.current + this.loadPerMinute;
		this.current = sum > this.capacity ? this.capacity : sum;
	}

	public void decrease(Double decrease){
		this.current -= decrease==null?0D:decrease;
	}

	public GeoJsonPoint getLocation() {
		return location;
	}

	public void setLocation(GeoJsonPoint location) {
		this.location = location;
	}

	public GroupStorageEquipment getGroupStorageEquipment() {
		return groupStorageEquipment;
	}

	public void setGroupStorageEquipment(GroupStorageEquipment groupStorageEquipment) {
		this.groupStorageEquipment = groupStorageEquipment;
	}
}
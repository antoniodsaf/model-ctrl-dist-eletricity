package br.antoniof.ctrl_dist_electricity.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author antonio
 */
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 8571261118900116242L;

    private String id;
    private Date createdAt;
    private Date updatedAt;

    public BaseEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(final Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
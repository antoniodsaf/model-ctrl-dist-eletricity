package br.antoniof.ctrl_dist_electricity.model;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author antonio
 */
public class Authority implements GrantedAuthority {

    @Id
    private String id;
    private String name;

    public String getAuthority() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
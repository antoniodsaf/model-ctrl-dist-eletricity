package br.antoniof.ctrl_dist_electricity.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;

/**
 * @author antonio
 */
@Document(collection = "groupStorageEquipaments")
public class GroupStorageEquipment {

    @Id
    private String id;
    @DBRef
    private Collection<StorageEquipment> storageEquipments;
    private String name;
    private String description;
    private GeoJsonPolygon locations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Collection<StorageEquipment> getStorageEquipments() {
        return storageEquipments;
    }

    public void setStorageEquipments(Collection<StorageEquipment> storageEquipments) {
        this.storageEquipments = storageEquipments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GeoJsonPolygon getLocations() {
        return locations;
    }

    public void setLocations(GeoJsonPolygon locations) {
        this.locations = locations;
    }
}